package djo;

//Robo indep

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.awt.geom.Point2D.Double;
import java.util.Enumeration;
import java.util.Hashtable;
import robocode.AdvancedRobot;
import robocode.HitByBulletEvent;
import robocode.HitRobotEvent;
import robocode.HitWallEvent;
import robocode.RobotDeathEvent;
import robocode.ScannedRobotEvent;
import robocode.util.Utils;

class FieldRobot {
	public Point2D.Double ultimaPos;
	public double energia;
	public boolean alive;
	public double ultimoAbsBearingRadians;
	public double distancia;
}

public class TesteGF extends AdvancedRobot{

	long fireTime;
	double bulletPower;
	boolean temAlvo;

	public TesteGF() {
		this.bulletPower = 2;
		this.temAlvo = false;
		this.fireTime = -1;
	}

	
	Hashtable robos;				//Lista de todos os robos no campo
	FieldRobot alvo;				//Robo mais perigoso no momento
	Point2D.Double objetivo;		//Pra onde se quer andar
	Rectangle2D.Double arena;		//Dimensoes do campo para onde da pra se andar
	Point2D.Double centro;			//Coordenadas do centro pta tentar se afastar dele
	Point2D.Double lastPosition;
	static Point2D.Double minhaPosicao;	//Duh
	//Ver se coloco a posicao onde eu tava antes de mudar de alvo/objetivo
	int scannedX = Integer.MIN_VALUE;
	int scannedY = Integer.MIN_VALUE;



	public void run() {
		setTurnRadarRight(java.lang.Double.POSITIVE_INFINITY);
		arena = new Rectangle2D.Double(50, 50, this.getBattleFieldWidth() - 100, this.getBattleFieldHeight() - 100);
		objetivo = new Point2D.Double(getX(), getY());
		robos = new Hashtable();
		alvo = new FieldRobot();
		lastPosition = minhaPosicao = objetivo;
		centro = new Point2D.Double(arena.getCenterX(), arena.getCenterY());
		do {
			if(fireTime == getTime()) {
				if(Utils.isNear(getGunTurnRemainingRadians(), 0)) {
					fire(bulletPower);
					this.temAlvo = false;
				} else {
					++this.fireTime;
				}
			}
			if(getTime() > 10) {
				this.tiroMovimento();
			}
			execute();

		} while(true);
	}


	public void tiroMovimento() {
		double distanciaAlvo = alvo.ultimaPos.distance(minhaPosicao);
		double distanciaObjetivo = objetivo.distance(new Point2D.Double(getX(), getY()));

		//Tiro



		//Movimento
		if(distanciaObjetivo < 50) {
			//Calcular o proximo objetivo
			this.calcularObjetivo();
			lastPosition = minhaPosicao;

		} else {

			//Nao entendo isso, mas so se move +- bem assim...wtf
			double angle = Math.atan2(objetivo.x - getX(), objetivo.y - getY()) - getHeadingRadians();
			double direction = 1;

			if(Math.cos(angle) < 0) {
				angle += Math.PI;
				direction = -1;
			}

			setAhead(distanciaObjetivo * direction);
			setTurnRightRadians(angle = Utils.normalRelativeAngle(angle));
			// hitting walls isn't a good idea, but HawkOnFire still does it pretty often
			setMaxVelocity(Math.abs(angle) > 1 ? 0 : 8d);
			//ate aki

			scannedX = (int)(objetivo.x);
			scannedY = (int)(objetivo.y);
		}
	}

	public void calcularObjetivo() {
		Point2D.Double ponto;

		boolean parado = true;
		//gerar 150 pontos ao seu redor, sem considerar ainda o centro;
		for(int i = 0; i < 150; i++) {
			ponto = new Point2D.Double(minhaPosicao.x + (100+150*Math.random())*Math.sin(2*Math.PI*Math.random()), 
					minhaPosicao.y + (100+150*Math.random())*Math.cos(2*Math.PI*Math.random()));
			if(avaliacao(ponto) < avaliacao(this.objetivo)) {
				if(arena.contains(ponto)) {
					objetivo = (Double) ponto;
					parado = false;
				}
			}
		}
		if(objetivo.distance(new Point2D.Double(getX(), getY())) < 30){
			do {
				ponto = new Point2D.Double(minhaPosicao.x + (100+100*Math.random())*Math.sin(2*Math.PI*Math.random()), 
						minhaPosicao.y + (100+100*Math.random())*Math.cos(2*Math.PI*Math.random()));
			} while(!arena.contains(ponto));
		}

	}

	public double avaliacao(Point2D.Double p) {
		Enumeration en = robos.elements();
		double retorno = 0.0;
		double antiGravidadeUltimaposicao = 1 - Math.rint(Math.pow(Math.random(), getOthers()));
		retorno += antiGravidadeUltimaposicao*0.08/p.distanceSq(lastPosition);//ele n ficou mais parando feito um retard...okay ne
		while(en.hasMoreElements()) {
			FieldRobot robot = (FieldRobot)en.nextElement();
			double razaoenergia = Math.min(robot.energia/getEnergy(), 2);
			double anti_gravidade = 1 + //p.distance(robot.ultimaPos);
					Math.abs(Math.cos(Math.atan2(minhaPosicao.x - p.x, minhaPosicao.y - p.y)//dafuq essa parte 
							- Math.atan2(robot.ultimaPos.x - p.x, robot.ultimaPos.y - p.y)))//dafuq essa parte
							/robot.ultimaPos.distanceSq(p);
			retorno += razaoenergia * anti_gravidade;
		}
		retorno += 
				retorno += 1/centro.distanceSq(p);
		return retorno;
	}

	public void onScannedRobot(ScannedRobotEvent e) {
		FieldRobot inimigo = (FieldRobot)robos.get(e.getName());

		if(inimigo == null) {
			inimigo = new FieldRobot();
			robos.put(e.getName(), inimigo);
		}

		inimigo.alive = true;
		inimigo.distancia = e.getDistance();
		inimigo.energia = e.getEnergy();
		inimigo.ultimoAbsBearingRadians = e.getBearingRadians() + getHeadingRadians();
		inimigo.ultimaPos = new Point2D.Double(getX() + inimigo.distancia*Math.sin(inimigo.ultimoAbsBearingRadians),
				getY() + inimigo.distancia*Math.cos(inimigo.ultimoAbsBearingRadians));

		if(!e.getName().contains("Equipe") && (inimigo.distancia < alvo.distancia || !alvo.alive)) 
			alvo = inimigo;
		
		if(!this.temAlvo) {
			double headOnBearing = getHeadingRadians() + e.getBearingRadians();
			double linearBearing = headOnBearing + Math.asin(e.getVelocity() / 14 * Math.sin(e.getHeadingRadians() - headOnBearing));
			setTurnGunRightRadians(Utils.normalRelativeAngle(linearBearing - getGunHeadingRadians()));
			fireTime = getTime() + 1;
			System.out.println("vou girar. alvo = "+e.getName());
			this.temAlvo = true;
		}
	}

	public void onHitRobot(HitRobotEvent e) {
		this.calcularObjetivo();
		lastPosition = minhaPosicao;

	}

	public void onHitByBullet(HitByBulletEvent e) {

	}

	public void onRobotDeath(RobotDeathEvent e) {
		((FieldRobot)robos.get(e.getName())).alive = false;//Deletar o robo em algum lugar. tvz deixar o "fantasma" do inimigo sirva pro movimento aloka
		robos.remove(e.getName());
	}

	public void onPaint(Graphics2D g) {
		// Set the paint color to a red half transparent color
		g.setColor(new Color(0xff, 0x00, 0x00, 0x80));

		// Draw a line from our robot to the scanned robot
		g.drawLine(scannedX, scannedY, (int)getX(), (int)getY());

		// Draw a filled square on top of the scanned robot that covers it
		g.fillRect(scannedX - 20, scannedY - 20, 40, 40);

		g.drawOval(scannedX-50, scannedY-50, 100, 100);

		//		g.setColor(new Color(0xdd, 0x34, 0xaa, 0xff));
		//		g.fillOval(scannedX-45, scannedY-45, 90, 90);
	}
}
